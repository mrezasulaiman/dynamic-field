package com.example.dynamicfield;

import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by M Reza Sulaiman on 10,January,2020
 * Jakarta, Indonesia.
 */
public class Component {
    private String name;
    private String value;
    private String type;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
