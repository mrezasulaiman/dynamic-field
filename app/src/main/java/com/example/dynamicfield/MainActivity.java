package com.example.dynamicfield;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;




public class MainActivity extends AppCompatActivity {

        Button button;
        Button button_post;
        private LinearLayout linearLayout;

        RFields rFields;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.json_btn);
        button_post = findViewById(R.id.post_btn);

        rFields = new RFields(getActivityContext(),MainActivity.class);

        linearLayout = findViewById(R.id.relative_layout);

        final String jsonString =
                        "[{\"name\":\"nama\",\"value\":\"reza\",\"type\":\"String\"}," +
                        "{\"name\":\"umur\",\"value\":\"22\",\"type\":\"number\"}," +
                        "{\"name\":\"tanggal\",\"value\":\"22/01/2020\",\"type\":\"date\"}]";

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("OnClick()","clicked");
                rFields.buildFields(jsonString);
                Log.d("Onclick()","rFields value: "+ rFields);
                linearLayout.addView(rFields.getContent());
                Log.d("OnClick()","rFields Content(): " + rFields.getContent());
                rFields.setContent(linearLayout);
            }
        });

        button_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rFields.getDataJson();
            }
        });
    }


    private Context getActivityContext() {
        return this;
    }
}
