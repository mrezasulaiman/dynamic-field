package com.example.dynamicfield;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * THIS.ID
 * Created by M Reza Sulaiman on 14,January,2020
 * Jakarta, Indonesia.
 */
public class RFields {

    private LinearLayout content;
    private  EditText editText;
    private  EditText editTextNumber;
    private  EditText editTextDate;
    private final Context context;
    private Class<?> className;
    private LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    private int MARGIN_LEFT = 90;
    private int MARGIN_TOP = 0;
    private int MARGIN_RIGHT = 90;
    private int MARGIN_BOTTOM = 0;
    private int SPACING_LEFT = 0;
    private int SPACING_TOP = 20;
    private int SPACING_RIGHT = 0;
    private int SPACING_BOTTOM = 0;

    private List<EditText> listEditText;


    /**
     * DEFINE RField with Context & Class Name
     *
     * @param context
     * @param className
     */
    public RFields(Context context, Class<?> className) {
        this.context = context;
        this.className = className;
        this.content = new LinearLayout(context);
        this.editText = new EditText(context);
        this.editTextNumber = new EditText(context);
        this.editTextDate = new EditText(context);
        this.listEditText = new ArrayList<>();
        content.setOrientation(LinearLayout.VERTICAL);
        setMargin(true);
        setSpacing(true);

    }

    /**
     * RENDER DYNAMIC FIELD FROM JSON ARRAY
     * @param jsonFields
     */
    public void buildFields(String jsonFields) {
        try {
            JSONArray jsonArray = new JSONArray(jsonFields);
            Moshi moshi = new Moshi.Builder().build();
            Type type = Types.newParameterizedType(List.class, Component.class);
            JsonAdapter<List<Component>> adapter = moshi.adapter(type);
            List<Component> components = adapter.fromJson(jsonArray.toString());
            Log.d("OnClick()", "components value: " + components);

            for (Component c : components) {
                Log.d("Onclick()", "component name: " + c.getName());
                Log.d("Onclick()", "component value: " + c.getValue());
                Log.d("Onclick()", "component type: " + c.getType());

                if (c.getType().equals("String")) {
                    Log.d("Onclick()", "component type: " + c.getType());
                    addField(c.getValue());
                } else if (c.getType().equals("date")) {
                    Log.d("Onclick()", "component type: " + c.getType());
                    addDatePicker(c.getValue());
                } else if (c.getType().equals("number")) {
                    Log.d("Onclick()", "component type: " + c.getType());
                    addFieldNumber(c.getValue());
                }
            }
        } catch (Throwable t) {
            Log.e("My App", t.getMessage());
        }
    }

    /**
     * RENDER TEXT FIELD
     * @param value
     */
    private void addField(String value) {
        editText.setId(0);
//        editText.setTag("String");
        editText.setLayoutParams(params);
        editText.setTextColor(0xff000000);
        editText.setText(value);
        listEditText.add(editText);
        content.addView(editText);
    }

    /**
     * RENDER TEXT FIELD NUMBER
     * @param value
     */
    @SuppressLint("ResourceType")
    private void addFieldNumber(String value) {
        editTextNumber.setId(1);
//        editTextNumber.setTag("number");
        editTextNumber.setLayoutParams(params);
        editTextNumber.setText(value);
        editTextNumber.setTextColor(0xff000000);
        editTextNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        listEditText.add(editTextNumber);
        content.addView(editTextNumber);
    }

    /**
     * RENDER DATE PICKER
     * @param value
     */
    @SuppressLint("ResourceType")
    private void addDatePicker(String value) {
        editTextDate.setId(2);
//        editTextDate.setTag("date");
        editTextDate.setLayoutParams(params);
        editTextDate.setTextColor(0xff000000);
        editTextDate.setHint("Date");
        editTextDate.setFocusable(false);
        editTextDate.setText(value);
        content.addView(editTextDate);
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear;
                int mMonth;
                int mDay;
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                editTextDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                listEditText.add(editTextDate);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    /**
     * DEFAULT MARGIN
     * @param enable
     */
    public void setMargin(boolean enable) {
        if (enable) {
            content.setPadding(MARGIN_LEFT, MARGIN_TOP, MARGIN_RIGHT, MARGIN_BOTTOM);
        } else {
            content.setPadding(0, 0, 0, 0);
        }
    }

    /**
     * HOW TO CUSTOM MARGIN
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public void setMargin(int left, int top, int right, int bottom) {
        content.setPadding(left, top, right, bottom);
    }

    /**
     * DEFAULT SPACING
     * @param enable
     */
    public void setSpacing(boolean enable) {
        if (enable) {
            params.setMargins(SPACING_LEFT,SPACING_TOP,SPACING_RIGHT,SPACING_BOTTOM);
        } else {
            content.setPadding(0, 0, 0, 0);
        }
    }

    /**
     * HOW TO CUSTOM SPACING
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public void setSpacing(int left, int top, int right, int bottom){
        params.setMargins(left, top, right, bottom);
    }

    /**
     * Get Form data as JSON
     * @return
     */
    public String getDataJson(){
        List<Component> components = new ArrayList<>();
        createComponent(components);

        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, Component.class);
        JsonAdapter<List<Component>> adapter = moshi.adapter(type);
        String json = adapter.toJson(components);
        Log.d("json","value: " + json);

        return json;
    }


    /**
     * Get Form Data as List
     * @return
     */
    public List<Component> getData(){
        List<Component> components = new ArrayList<>();
        createComponent(components);

        return components;
    }


    /**
     * Create Component
     * @param components
     */
    private void createComponent(List<Component> components ){
        for (EditText c : listEditText) {
            Component comp = new Component();
            comp.setName(String.valueOf(c.getId()));
//            comp.setType(c.getTag().toString());
            comp.setValue(c.getText().toString());
            components.add(comp);
        }
    }



    public LinearLayout getContent() {
        return content;
    }

    public void setContent(LinearLayout content) {
        this.content = content;
    }

}


